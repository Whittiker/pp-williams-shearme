import unittest  #, vending

from vending import *


class vending_test(unittest.TestCase):
    def setUp(self):
        print('Start before the test')

    def tearDown(self):
        print('Print after the test')

    def test_get_current_amount(self):
        vending = vending_machine()
        self.assertEqual(vending.get_current_amount, 0)

    def test_insert_coins(self):
        coins = {'Nickles':1, 'Dimes':2, 'Quarters': 3, 'Pennies':4}
        vending = vending_machine()
        vending.insert_coins(coins)
        self.assertEqual(vending.get_current_amount, 1)

if __name__ == '__main__':
    unittest.main()