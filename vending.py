class vending_machine(object):
    def __init__(self):
        self.current_amount = 0
        self.products = {}
        self.accepted_coins = {'Nickles':0.05, 'Dimes':0.10, 'Quarters': 0.25}
        self.total = 0

    def insert_coins(self, coins: dict):
        for coin in coins:
            if coin in self.accepted_coins:
                self.current_amount += coins[coin] * self.accepted_coins[coin]
            else:
                print("You can't enter {}s!".format(coin))
        print("You have entered ${}".format(self.current_amount))

    @property
    def get_current_amount(self):
        return self.current_amount

    def add_products(self):
        pass


    def display_products(self):
        pass


    def buy_product(self):
        pass


    def return_coins(self):
        pass
